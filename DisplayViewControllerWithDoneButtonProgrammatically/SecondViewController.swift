import UIKit

class SecondViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func showControllerAction(_ sender: UIButton) {
        if let thirdViewController = self.storyboard?.instantiateViewController(withIdentifier: "ThirdViewControllerID") {
            let navigationController = UINavigationController(rootViewController: thirdViewController)
            self.navigationController?.present(navigationController, animated: false, completion: {
                NSLog("Poping to root view controller")
                _ = self.navigationController?.popToRootViewController(animated: false)
            })
        }
    }

}

